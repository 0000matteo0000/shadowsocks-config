#!/bin/sh
set -e
# DEPENDENCIES
sudo apt-get update -qq
sudo apt-get install -qq jq tar wget
# STOP PREVIOUS SERVICE IF PRESENT
sudo systemctl stop "shadowsocks.local.service" || :
# CONFIG DIRECTORY
sudo mkdir -p -m 755 "/etc/shadowsocks"
cd "/etc/shadowsocks"
# GET BINARY
wget --quiet -O - "https://api.github.com/repos/shadowsocks/shadowsocks-rust/releases/latest" | \
jq -r '.assets[] | select(.name | test("^shadowsocks-v.*?\\.x86_64-unknown-linux-gnu\\.tar\\.xz$")) | .browser_download_url' | \
wget --quiet -O - --continue --input-file - | \
sudo tar --directory "/etc/shadowsocks" -x --xz --overwrite
wget --quiet -O - "https://api.github.com/repos/shadowsocks/v2ray-plugin/releases/latest" | \
jq -r '.assets[] | select(.name | test("^v2ray-plugin-linux-amd64-v.*?\\.tar\\.gz$")) | .browser_download_url' | \
wget --quiet -O - --continue --input-file - | \
sudo tar --directory "/etc/shadowsocks" -x --gzip --overwrite
# SERVER URI FROM PARAMETER 1
uri="$1"
j="$("/etc/shadowsocks/ssurl" --decode "$uri")"
ip="$(echo $j | jq -r ".server")"
port="$(echo $j | jq -r ".server_port")"
password="$(echo $j | jq -r ".password")"
method="$(echo $j | jq -r ".method")"
plugin_options="fast-open=true;loglevel=none"
# plugin_options="tls;fast-open=true;loglevel=none"  # tls breaks: `Secure Connection Failed` `PR_END_OF_FILE_ERROR` on browser
local_address="127.0.0.1"
local_port="1080"
cat <<EOT | sudo tee "/etc/shadowsocks/shadowsocks.local.json" > /dev/null
{
    "server": "$ip",
    "server_port": $port,
    "password": "$password",
    "method": "$method",
    "dns": "cloudflare",
    "mode": "tcp_and_udp",
    "fast_open": true,
    "no_delay": true,
    "plugin": "/etc/shadowsocks/v2ray-plugin_linux_amd64",
    "plugin_opts": "$plugin_options",
    "protocol": "socks",
    "local_address": "$local_address",
    "local_port": $local_port
}
EOT
# "ipv6_first": true  # breaks: `Secure Connection Failed` `PR_END_OF_FILE_ERROR` on browser if ipv6 is not supported by the server network and is enabled in the server config.
# SERVICE USER IF NOT PRESENT
sudo useradd --no-create-home --system shadowsocks || :
# SERVICE
cat <<EOT | sudo tee "/etc/shadowsocks/shadowsocks.local.service" > /dev/null
[Unit]
Description=shadowsocks-rust Server Service
After=network-online.target
[Service]
User=shadowsocks
Group=shadowsocks
Type=simple
ExecStart=/etc/shadowsocks/sslocal --log-without-time --config "/etc/shadowsocks/shadowsocks.local.json"
Restart=on-failure
[Install]
WantedBy=multi-user.target
EOT
sudo ln -sf "/etc/shadowsocks/shadowsocks.local.service" "/etc/systemd/system/shadowsocks.local.service"
# START SERVICE
sudo systemctl daemon-reload
sudo systemctl enable --now "shadowsocks.local.service"
# SHOW SEVRICE LOGS
echo 'Run `sudo journalctl --pager-end --follow --unit="shadowsocks.local.service"` if you wish to se the service logs.'
echo "Setup your proxy to listen to address $local_address on port $local_port with SOCKS5 mode."
# sudo journalctl --pager-end --follow --unit="shadowsocks.local.service"

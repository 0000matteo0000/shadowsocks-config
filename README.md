# Run on the server
## To install:
`sudo apt-get install -qq curl && sh -c "$(curl -sSL "https://gitlab.com/0000matteo0000/shadowsocks-config/-/raw/client-service/server.sh")"`
## After installation:
the service will automatically start.
# Run on the client
## To install:
`sudo apt-get install -qq curl && sh -c "$(curl -sSL "https://gitlab.com/0000matteo0000/shadowsocks-config/-/raw/client-service/client.sh")" -- '<uri (ss://...)>'`
## After installation:
the service will automatically start.
# Warning
These scripts only support Debian-based distributions

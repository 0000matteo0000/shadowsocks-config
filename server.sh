#!/bin/sh
set -e
# CONFIGURATION PARAMETERS
port="8388"  # port to run the server on
method="chacha20-ietf-poly1305"  # cypher method to use
password="$(openssl rand -base64 32)"  # password used for encryption
ip="$(dig @resolver4.opendns.com myip.opendns.com +short)"  # the ip of the server
# DEPENDENCIES
sudo apt-get update -qq
sudo apt-get install -qq dnsutils jq net-tools openssl tar wget
# STOP PREVIOUS SERVICE IF PRESENT
sudo systemctl stop "shadowsocks.service" || :
# SERVICE USER IF NOT PRESENT
sudo useradd --no-create-home --system shadowsocks || :
# CONFIG DIRECTORY
sudo mkdir -p -m 755 "/etc/shadowsocks"
cd "/etc/shadowsocks"
# GET BINARY
wget --quiet -O - "https://api.github.com/repos/shadowsocks/shadowsocks-rust/releases/latest" | \
jq -r '.assets[] | select(.name | test("^shadowsocks-v.*?\\.x86_64-unknown-linux-gnu\\.tar\\.xz$")) | .browser_download_url' | \
wget --quiet -O - --continue --input-file - | \
sudo tar --directory "/etc/shadowsocks" -x --xz --overwrite
wget --quiet -O - "https://api.github.com/repos/shadowsocks/v2ray-plugin/releases/latest" | \
jq -r '.assets[] | select(.name | test("^v2ray-plugin-linux-amd64-v.*?\\.tar\\.gz$")) | .browser_download_url' | \
wget --quiet -O - --continue --input-file - | \
sudo tar --directory "/etc/shadowsocks" -x --gzip --overwrite
# FIREWALL RULES
sudo iptables -A "INPUT" -p "tcp" --dport "$port" -m "comment" --comment "Shadowsocks" -j "ACCEPT"
sudo iptables -A "INPUT" -p "udp" --dport "$port" -m "comment" --comment "Shadowsocks" -j "ACCEPT"
# # CERTIFICATE  # for tls
# sudo openssl req -x509 -newkey ED25519 -keyout "/etc/shadowsocks/key.pem" -out "/etc/shadowsocks/cert.pem" -days 3650 --nodes -subj "/CN=$ip"
# sudo chown "shadowsocks:shadowsocks" "key.pem" "cert.pem"
# sudo chmod 400 "key.pem" "cert.pem"
# PLUGIN OPTIONS
plugin_options="fast-open=true;loglevel=none"
# tls  # breaks: http: TLS handshake error from <ip>:<random_port>: remote error: tls: bad certificate
# mode=quic  # breaks: failed to start server: v2ray.com/core/app/proxyman/inbound: failed to listen TCP on 8388 > v2ray.com/core/transport/internet: failed to listen on address: 0.0.0.0:8388 > listen udp 0.0.0.0:8388: bind: address already in use
# CONFIG FILE
cat <<EOT | sudo tee "/etc/shadowsocks/shadowsocks.json" > /dev/null
{
    "server": "0.0.0.0",
    "server_port": $port,
    "password": "$password",
    "method": "$method",
    "dns": "cloudflare",
    "mode": "tcp_and_udp",
    "fast_open": true,
    "no_delay": true,
    "plugin": "/etc/shadowsocks/v2ray-plugin_linux_amd64",
    "plugin_opts": "server;$plugin_options"
}
EOT
# "ipv6_first": true  # breaks: `ERROR tcp tunnel 127.0.0.1:<random_port> -> <random_site>:443 connect failed, error: Network is unreachable (os error 101)` if ipv6 is not supported by the server network.
# "plugin_opts": "server;cert=/etc/shadowsocks/cert.pem;key=/etc/shadowsocks/key.pem;$plugin_options"  # for tls
# SERVICE
cat <<EOT | sudo tee "/etc/shadowsocks/shadowsocks.service" > /dev/null
[Unit]
Description=shadowsocks-rust Server Service
After=network-online.target
[Service]
User=shadowsocks
Group=shadowsocks
Type=simple
ExecStart=/etc/shadowsocks/ssserver --log-without-time --config "/etc/shadowsocks/shadowsocks.json"
Restart=on-failure
[Install]
WantedBy=multi-user.target
EOT
sudo ln -sf "/etc/shadowsocks/shadowsocks.service" "/etc/systemd/system/shadowsocks.service"
# START SERVICE
sudo systemctl daemon-reload
sudo systemctl enable --now "shadowsocks.service"
# DONE
uri="ss://$(echo -n "$method:$password" | base64 --wrap=0 | tr "/+" "_-" | tr -d "=")@$ip:$port#shadowsocks.proxy"
# uri="ss://$(echo -n "$method:$password" | base64 --wrap=0 | tr "/+" "_-" | tr -d "=")@$ip:$port/?pugin=v2ray-plugin&plugin_opts=$plugin_options#shadowsocks.proxy"
echo "Server uri: '$uri'"
# SHOW SEVRICE LOGS
echo 'Run `sudo journalctl --pager-end --follow --unit="shadowsocks.service"` if you wish to se the service logs.'
# sudo journalctl --pager-end --follow --unit="shadowsocks.service"
